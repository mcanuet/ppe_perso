-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: mcanuet
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppe_1_utilisateur`
--

DROP TABLE IF EXISTS `ppe_1_utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppe_1_utilisateur` (
  `utilisateur_num` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_identifiant` varchar(45) NOT NULL,
  `utilisateur_admin` tinyint(4) NOT NULL,
  `utilisateur_responssable` tinyint(4) NOT NULL DEFAULT '0',
  `utilisateur_mdp` varchar(45) NOT NULL,
  `ligue_num` int(11) DEFAULT NULL,
  `utilisateur_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`utilisateur_num`),
  UNIQUE KEY `utilisateur_num_UNIQUE` (`utilisateur_num`),
  UNIQUE KEY `utilisateur_identifiant_UNIQUE` (`utilisateur_identifiant`),
  KEY `fk_ppe_1_utilisateur_ppe_1_ligue1_idx` (`ligue_num`),
  CONSTRAINT `fk_ppe_1_utilisateur_ppe_1_ligue1` FOREIGN KEY (`ligue_num`) REFERENCES `ppe_1_ligue` (`ligue_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppe_1_utilisateur`
--

LOCK TABLES `ppe_1_utilisateur` WRITE;
/*!40000 ALTER TABLE `ppe_1_utilisateur` DISABLE KEYS */;
INSERT INTO `ppe_1_utilisateur` VALUES (1,'admin1',1,0,'d033e22ae348aeb5660fc2140aec35850c4da997',NULL,NULL),(2,'user1',0,1,'12dea96fec20593566ab75692c9949596833adc9',1,NULL),(3,'user2',0,1,'12dea96fec20593566ab75692c9949596833adc9',2,NULL),(4,'user3',0,1,'12dea96fec20593566ab75692c9949596833adc9',3,NULL);
/*!40000 ALTER TABLE `ppe_1_utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ppe_1_salle`
--

DROP TABLE IF EXISTS `ppe_1_salle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppe_1_salle` (
  `salle_num` int(11) NOT NULL AUTO_INCREMENT,
  `salle_nom` varchar(45) NOT NULL,
  `salle_places` int(11) NOT NULL DEFAULT '15',
  `salle_informatise` tinyint(4) NOT NULL DEFAULT '0',
  `salle_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`salle_num`),
  UNIQUE KEY `salle_num_UNIQUE` (`salle_num`),
  UNIQUE KEY `salle_nom_UNIQUE` (`salle_nom`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppe_1_salle`
--

LOCK TABLES `ppe_1_salle` WRITE;
/*!40000 ALTER TABLE `ppe_1_salle` DISABLE KEYS */;
INSERT INTO `ppe_1_salle` VALUES (1,'ni_30_1',30,0,NULL),(2,'ni_30_2',30,0,NULL),(3,'ni_30_3',30,0,NULL),(5,'ni_30_4',30,0,NULL),(6,'ni_30_5',30,0,NULL),(7,'ni_18_1',18,0,NULL),(8,'ni_18_2',18,0,NULL),(9,'ni_18_3',18,0,NULL),(10,'ni_18_4',18,0,NULL),(11,'ni_18_5',18,0,NULL),(12,'i_18_1',18,1,NULL),(13,'i_18_2',18,1,NULL),(14,'i_18_3',18,1,NULL),(15,'i_18_4',18,1,NULL),(16,'i_18_5',18,1,NULL);
/*!40000 ALTER TABLE `ppe_1_salle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ppe_1_reservation`
--

DROP TABLE IF EXISTS `ppe_1_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppe_1_reservation` (
  `reservation_num` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_demande` datetime NOT NULL,
  `reservation_debut` datetime NOT NULL,
  `reservation_fin` datetime NOT NULL,
  `salle_num` int(11) NOT NULL,
  `ligue_num` int(11) NOT NULL,
  PRIMARY KEY (`reservation_num`,`salle_num`,`ligue_num`),
  UNIQUE KEY `reservation_num_UNIQUE` (`reservation_num`),
  KEY `fk_ppe_1_reservation_ppe_1_salle1_idx` (`salle_num`),
  KEY `fk_ppe_1_reservation_ppe_1_ligue1_idx` (`ligue_num`),
  CONSTRAINT `fk_ppe_1_reservation_ppe_1_ligue1` FOREIGN KEY (`ligue_num`) REFERENCES `ppe_1_ligue` (`ligue_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ppe_1_reservation_ppe_1_salle1` FOREIGN KEY (`salle_num`) REFERENCES `ppe_1_salle` (`salle_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppe_1_reservation`
--

LOCK TABLES `ppe_1_reservation` WRITE;
/*!40000 ALTER TABLE `ppe_1_reservation` DISABLE KEYS */;
INSERT INTO `ppe_1_reservation` VALUES (1,'2019-05-21 22:00:10','2019-05-21 08:00:00','2019-05-21 09:00:00',1,1);
/*!40000 ALTER TABLE `ppe_1_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ppe_1_ligue`
--

DROP TABLE IF EXISTS `ppe_1_ligue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppe_1_ligue` (
  `ligue_num` int(11) NOT NULL AUTO_INCREMENT,
  `ligue_nom` varchar(45) NOT NULL,
  `ligue_sport` varchar(45) NOT NULL,
  `ligue_tel` varchar(45) NOT NULL,
  `ligue_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`ligue_num`),
  UNIQUE KEY `ligue_num_UNIQUE` (`ligue_num`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppe_1_ligue`
--

LOCK TABLES `ppe_1_ligue` WRITE;
/*!40000 ALTER TABLE `ppe_1_ligue` DISABLE KEYS */;
INSERT INTO `ppe_1_ligue` VALUES (1,'ligue%20de%20foot','foot','0123456789',NULL),(2,'ligue%20de%20hockey','hockey','0213456789',NULL),(3,'ligue%20de%20curling','curling','0312456789',NULL),(4,'ligue%20de%20ponney','%C3%A9quitation','0412356789',NULL),(5,'ligue%20d\'aviron','aviron','0512346789',NULL);
/*!40000 ALTER TABLE `ppe_1_ligue` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-21 22:23:59

<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxime
 * Date: 22/10/2018
 * Time: 16:25
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Connexion extends CI_Model {

    /**
     * @param $pseudo string le nom de l'utilisateur
     * @param $mdp    string mot de passe de l'utilisateur
     * @return        array  un tableau qui correspond contenant les utilisateurs avec le couple pseudo/mdp correspondant
     */
    public function test($pseudo, $mdp){
        return $this->db->select('utilisateur_num, utilisateur_identifiant, utilisateur_admin, utilisateur_responssable, utilisateur_verr, ligue_num')
            ->from('ppe_1_utilisateur')
            ->where('utilisateur_identifiant',$pseudo)
            ->where('utilisateur_mdp',sha1($mdp))
            ->get()->result();
    }
}

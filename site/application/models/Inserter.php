<?php

class Inserter extends CI_Model {

  /**
   * @param string $pseudo
   * @param string $pw
   * @param null   $ligue
   * @param int    $isResponssable
   * @param int    $isAdmin
   */
  public function createUser($pseudo, $pw, $ligue=null, $isResponssable=0, $isAdmin=0){
    $data = array(
      'utilisateur_identifiant' => $pseudo,
      'utilisateur_mdp' => $pw,
      'ligue_num' => $ligue,
      'utilisateur_admin' => $isAdmin,
      'utilisateur_responssable' => $isResponssable
    );
    $this->db->insert('ppe_1_utilisateur', $data);
  }

  /**
   * @param string $nom
   * @param string $sport
   * @param string $tel
   */
  public function createLigue($nom , $sport, $tel){
    $data = array(
      'ligue_nom' => $nom,
      'ligue_sport' => $sport,
      'ligue_tel' => $tel
    );
    $this->db->insert('ppe_1_ligue', $data);
  }

  /**
   * @param string $nom
   * @param string $places
   * @param int    $informatisee
   */
  public function createSalle($nom , $places, $informatisee=0){
    $data = array(
      'salle_nom' => $nom,
      'salle_places' => $places,
      'salle_informatise' => $informatisee
    );
    $this->db->insert('ppe_1_salle', $data);
  }

    /**
     * @param int      $info
     * @param string   $debut
     * @param string   $fin
     * @return boolean
     */
  public function makeReservation($info, $debut, $fin, $places) {
      try{
          $this->db->query('call resSalle('.$debut.', '.$fin.', '.$info.', '.$this->session->user['LigueId'].','.$places.')');
          return true;
      } catch (Exeption $e){
          return false;
      }
  }
}

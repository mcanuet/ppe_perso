<?php

class Setter extends CI_Model {

  /**
   * permet de verrouiller une salle
   * @param int $id
   */
  public function verrSalle($id) {
    $this->db->where('salle_num', $id)->update('ppe_1_salle', array('salle_verr' => date("Y-m-d H:i:s")));
  }

  /**
   * permet de déverrouiller une salle
   * @param int $id
   */
  public function deverrSalle($id) {
    $this->db->where('salle_num', $id)->update('ppe_1_salle', array('salle_verr' => null));
  }

  /**
   * permet de verrouiller un utilisateur
   * @param int $id
   */
  public function verrUser($id) {
    $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_verr' => date("Y-m-d H:i:s")));
  }

  /**
   * permet de déverrouiller un utilisateur
   * @param int $id
   */
  public function deverrUser($id) {
    $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_verr' => null));
  }

    /**
     * permet à un utilisateur de ce connecter sur l'interface d'adiministration
     * @param int $id
     */
    public function setAdmin($id) {
        $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_admin' => 1));
    }

    /**
     * réfoque les droits administrateur d'un utilisateur
     * @param int $id
     */
    public function removeAdmin($id) {
        $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_admin' => 0));
    }

    /**
     * permet à un utilisateur d'avoir les droit du responssable de ligue
     * @param int $id
     */
    public function setResp($id) {
        $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_responssable' => 1));
    }

    /**
     * révoque les droit de responssable de ligue d'un utilisateur
     * @param int $id
     */
    public function removeResp($id) {
        $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur', array('utilisateur_responssable' => 0));
    }

  /**
   * permet de verrouiller une ligue
   * @param int $id
   */
  public function verrLigue($id) {
    $this->db->where('ligue_num', $id)->update('ppe_1_ligue', array('ligue_verr' => date("Y-m-d H:i:s")));
  }

  /**
   * permet de déverrouiller une ligue
   * @param int $id
   */
  public function deverrLigue($id) {
    $this->db->where('ligue_num', $id)->update('ppe_1_ligue', array('ligue_verr' => null));
  }

    /**
     * permet de mettre à jour un utilisateur
     * @param integer      $id
     * @param string       $identifiant
     * @param string|null  $mdp
     * @param boolean      $admin
     * @param boolean      $resp
     * @param integer      $ligue
     */
  public function updateUser($id, $identifiant, $mdp, $admin, $resp, $ligue){
      $data = array(
          'utilisateur_identifiant' => $identifiant,
          'utilisateur_admin' => $admin,
          'utilisateur_responssable' => $resp,
          'ligue_num' => $ligue
      );
      if ($mdp !== null || $mdp === "") {$data['utilisateur_mdp'] = $mdp;}
      $this->db->where('utilisateur_num', $id)->update('ppe_1_utilisateur',$data);
  }

    /**
     * permet de mettre à jour une salle
     * @param integer $id
     * @param string  $nom
     * @param string  $places
     * @param boolean $info
     */
    public function updateSalle($id, $nom, $places, $info){
        $data = array(
            'salle_nom' => $nom,
            'salle_places' => $places,
            'salle_informatise' => $info
        );
        $this->db->where('salle_num', $id)->update('ppe_1_salle',$data);
    }

    /**
     * permet de mettre à jour une ligue
     * @param integer $id
     * @param string  $nom
     * @param string  $sport
     * @param string  $tel
     */
    public function updateLigue($id, $nom, $sport, $tel){
        $data = array(
            'ligue_nom' => $nom,
            'ligue_sport' => $sport,
            'ligue_tel' => $tel
        );
        $this->db->where('ligue_num', $id)->update('ppe_1_ligue',$data);
    }

    /**
     * permet de mettre à jour une réservation
     * @param integer $id
     * @param string  $demande
     * @param string  $debut
     * @param string  $fin
     * @param integer $ligue
     * @param integer $salle
     */
    public function updateReservation($id, $debut, $fin, $ligue, $salle){
        $data = array(
            'reservation_demande' => date("Y-m-d H:i:s"),
            'reservation_debut' => $debut,
            'reservation_fin' => $fin,
            'ligue_num' => $ligue,
            'salle_num' => $salle
        );
        $this->db->where('reservation_num', $id)->update('ppe_1_reservation',$data);
    }
}

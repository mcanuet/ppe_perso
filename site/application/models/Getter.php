<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxime
 * Date: 23/10/2018
 * Time: 15:38
 */

class Getter extends CI_Model
{
  /**
   * @return mixed retourne toutes les salles
   */
  public function AllSalles(){
    return $this->db->select('*')->from('ppe_1_salle')->get()->result();
  }

  /**
   * @return mixed retourne toutes les ligues
   */
  public function AllLigues() {
    return $this->db->select('*')->from('ppe_1_ligue')->get()->result();
  }

  /**
   * @return mixed retourne tous les utilisateurs
   */
  public function AllUsers() {
    return $this->db->select('*')->from('ppe_1_utilisateur')->get()->result();
  }

  /**
   * @param $id int id de la salle
   * @return mixed retourne une salle
   */
  public function OneSalle($id){
      return $this->db->select('*')->from('ppe_1_salle')->where('salle_num',$id)->get()->result();
  }

  /**
   * @param $id int id de l'utilisateur
   * @return mixed retourne un utilisateur
   */
  public function OneUser($id){
      return $this->db->select('*')->from('ppe_1_utilisateur')->where('utilisateur_num',$id)->get()->result();
  }

  /**
   * @param $id int id de la ligue
   * @return mixed retourne une ligue
   */
  public function OneLigue($id){
    return $this->db->select('*')->from('ppe_1_ligue')->where('ligue_num',$id)->get()->result();
  }

  /**
   * @param $id int id ode la reservation
   * @return mixed retourne une reservation
   */
  public function OneReservation($id){
    return $this->db->select(' reservation_num, reservation_debut, reservation_fin, salle_nom')
      ->from('ppe_1_reservation')->join('ppe_1_salle', 'ppe_1_reservation.salle_num = ppe_1_salle.salle_num')
      ->where('reservation_num',$id)
      ->get()->result();
  }

  /**
   * @return mixed  retourne toutes les reservations
   */
  public function AllReservation(){
    return $this->db->select('reservation_num, reservation_debut, reservation_fin, salle_nom')
      ->from('ppe_1_reservation')->join('ppe_1_salle', 'ppe_1_reservation.salle_num = ppe_1_salle.salle_num')
      ->where('reservation_fin >', date("Y-m-d"))->order_by('reservation_debut', 'ASC')
      ->get()->result();
  }

  /**
   * @param $ligueId id de la ligue
   * @return mixed retourne toutes les reservation pour une ligue
   */
  public function AllReservationOneUser($ligueId){
    return $this->db->select('reservation_num, reservation_debut, reservation_fin, salle_nom')
      ->from('ppe_1_reservation')->join('ppe_1_salle', 'ppe_1_reservation.salle_num = ppe_1_salle.salle_num')
      ->where('ligue_num',$ligueId)->where('reservation_fin >', date("Y-m-d"))->order_by('reservation_debut', 'ASC')
      ->get()->result();
  }
}

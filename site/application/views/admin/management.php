<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>
<br>
<div class="row white-text">
    <div class="col s8 offset-s2">
        <ul class="tabs blue ligthen-2">
            <li class="tab col s5"><a href="#test1" class="black-text" id="update">modifier / supprimer</a></li>
            <li class="tab col s5 offset-s2"><a href="#test2" class="black-text" id="create">ajouter</a></li>
        </ul>
    </div>
    <div class="row">
<!--        -->
<!--        modifier / supprimer une entité-->
        <div id="test1" class="col s12">
            <div class="row col s8 offset-s2">
<!--                modifier une salle-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Modifier / Supprimer une salle</div>
                        <div class="row">
                            <form id="salle_update" class="update">
                                <div class="row">
                                    <div class="col s3 input-field">
                                        <select name="salle_num" id="salle_num">
                                            <option disabled selected>choix de la salle</option>
                                        </select>
                                        <label>La salle</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4 input-field">
                                        <label for="salle_name">nom de la salle</label>
                                        <input class="validate" type="text" name="salle_name" id="salle_name" required>
                                    </div>
                                    <div class="col s2 offset-s1 input-field">
                                        <label for="salle_num">nombre de place</label>
                                        <input class="validate" type="number" min="0" name="salle_place" id="salle_place" required>
                                    </div>
                                    <div class="col s3 offset-s1 input-field">
                                        <span>Informatisé</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="salle_info" id="salle_info">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect" id="send_salle" value="modifier">
                            </form>
                        </div>
                    </div>
                </div>
<!--                modifier / supprimer ligue-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Modifer / Supprimer une ligue</div>
                        <div class="row">
                            <form id="ligue_update" class="update">
                                <div class="row">
                                    <div class="col s3 input-field">
                                        <select name="ligue_num" class="user_ligue" id="ligue_num">
                                            <option disabled selected>choix de la ligue</option>
                                        </select>
                                        <label>La ligue</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s4 input-field">
                                        <label for="ligue_name">nom de la ligue</label>
                                        <input class="validate" type="text" name="ligue_name" id="ligue_name" required>
                                    </div>
                                    <div class="col s2 offset-s1 input-field">
                                        <label for="ligue_sport">sport pratiqué</label>
                                        <input class="validate" type="text" name="ligue_sport" id="ligue_sport" required>
                                    </div>
                                    <div class="col s3 offset-s1 input-field">
                                        <label for="ligue_tel">teléphone de contacte</label>
                                        <input class="validate" type="tel" name="ligue_tel" id="ligue_tel" required>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect" id="send_ligue" value="modifier">
                            </form>
                        </div>
                    </div>
                </div>
<!--                modifier / supprimer utilisateur-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Modifer / Supprimer un utilisateur</div>
                        <div class="row">
                            <form id="user_update" class="update">
                                <div class="row">
                                    <div class="col s3 input-field">
                                        <select name="user_num" id="user_num">
                                            <option disabled selected>choix de l'utilisateur</option>
                                        </select>
                                        <label>L'utilisateur</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s3 input-field">
                                        <label for="user_name">nom de l'utilisateur</label>
                                        <input class="validate" type="text" name="user_name" id="user_name" required>
                                    </div>
                                    <div class="col s3 input-field">
                                        <label for="user_mdp">mot de passe</label>
                                        <input class="validate" type="password" name="user_mdp" id="user_mdp">
                                    </div>
                                    <div class="col s2 input-field">
                                        <span>Admin</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="user_admin" id="user_admin">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s2 input-field">
                                        <span>Responssable</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="user_responssable" id="user_responssable">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s2 input-field">
                                        <select name="user_ligue" class="user_ligue" id="user_ligue_upd">
                                            <option value=null>pas de ligue</option>
                                        </select>
                                        <label>ligue de l'utilisateur</label>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect" id="send_user" value="modifier">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        -->
<!--        ajouter une entité-->
        <div id="test2" class="col s12">
            <div class="row col s8 offset-s2">
<!--                ajouter salle-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Ajouter une salle</div>
                        <div class="row">
                            <form id="salle" class="create">
                                <div class="row">
                                    <div class="col s4 input-field">
                                        <label for="salle_name">nom de la salle</label>
                                        <input class="validate" type="text" name="salle_name" id="salle_name" required>
                                    </div>
                                    <div class="col s2 offset-s1 input-field">
                                        <label for="salle_num">nombre de place</label>
                                        <input class="validate" type="number" min="0" name="salle_num" id="salle_num" required>
                                    </div>
                                    <div class="col s3 offset-s1 input-field">
                                        <span>Informatisé</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="salle_info" id="salle_info">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect submit" id="send_salle">
                            </form>
                        </div>
                    </div>
                </div>
<!--                ajouter ligue-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Ajouter une ligue</div>
                        <div class="row">
                            <form id="ligue" class="create">
                                <div class="row">
                                    <div class="col s4 input-field">
                                        <label for="ligue_name">nom de la ligue</label>
                                        <input class="validate" type="text" name="ligue_name" id="ligue_name" required>
                                    </div>
                                    <div class="col s2 offset-s1 input-field">
                                        <label for="ligue_sport">sport pratiqué</label>
                                        <input class="validate" type="text" name="ligue_sport" id="ligue_sport" required>
                                    </div>
                                    <div class="col s3 offset-s1 input-field">
                                        <label for="ligue_tel">teléphone de contacte</label>
                                        <input class="validate" type="tel" name="ligue_tel" id="ligue_tel" required>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect submit" id="send_ligue">
                            </form>
                        </div>
                    </div>
                </div>
<!--                ajouter utilisateur-->
                <div class="card blue darken-3">
                    <div class="card-content">
                        <div class="card-title">Ajouter un utilisateur</div>
                        <div class="row">
                            <form id="user" class="create">
                                <div class="row">
                                    <div class="col s3 input-field">
                                        <label for="user_name">nom de l'utilisateur</label>
                                        <input class="validate" type="text" name="user_name" id="user_name" required>
                                    </div>
                                    <div class="col s3 input-field">
                                        <label for="user_mdp">mot de passe</label>
                                        <input class="validate" type="password" name="user_mdp" id="user_mdp" required>
                                    </div>
                                    <div class="col s2 input-field">
                                        <span>Admin</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="user_admin" id="user_admin">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s2 input-field">
                                        <span>Responssable</span>
                                        <div class="switch">
                                            <label>
                                                Non
                                                <input type="checkbox" name="user_responssable" id="user_responssable">
                                                <span class="lever"></span>
                                                Oui
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s2 input-field">
                                        <select name="user_ligue" class="user_ligue">
                                            <option value=null>pas de ligue</option>
                                        </select>
                                        <label>ligue de l'utilisateur</label>
                                    </div>
                                </div>
                                <input type="submit" class="btn waves-effect submit" id="send_user">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="<?php echo site_url("/../assets/js/admin_management.js")?>"></script>


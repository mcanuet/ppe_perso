<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
  <div class="col s12">
    <div class="card blue lighten-3 col s4">
      <div class="row">
        <div class="card-content">
          <h4 class="card-title">Utilisateurs</h4>
          <table class="striped centered">
            <thead>
            <tr>
              <th>identifiant</th>
              <th>ligue</th>
              <th>administrateur</th>
              <th>responsable</th>
              <th>verrouiller</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($users as $user) {
              echo "<tr>";
              echo "<td>".$user->utilisateur_identifiant."</td>";
              echo "<td>".$user->ligue_num."</td>";
              echo '<td><div class="switch"><label><input class="userAdmin" id="user_'.$user->utilisateur_num.'" type="checkbox" '.($user->utilisateur_admin === "0" ? '': 'checked').'> <span class="lever"></span></label></div></td>';
              echo '<td><div class="switch"><label><input class="userResp"  id="user_'.$user->utilisateur_num.'" type="checkbox" '.($user->utilisateur_responssable === "0" ? '': 'checked').'> <span class="lever"></span></label></div></td>';
              echo '<td><div class="switch"><label><input class="userVerr"  id="user_'.$user->utilisateur_num.'" type="checkbox" '.($user->utilisateur_verr === null ? '': 'checked').'> <span class="lever"></span></label></div></td>';
              echo "</tr>";
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="card blue lighten-3 col s4">
      <div class="row">
        <div class="card-content">
          <h4 class="card-title">Ligues</h4>
          <table class="striped centered">
            <thead>
            <tr>
              <th>numéro</th>
              <th>nom</th>
              <th>sport</th>
              <th>contact</th>
              <th>verrouiller</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($ligues as $ligue) {
              echo "<tr>";
              echo "<td>".$ligue->ligue_num."</td>";
              echo "<td>".$ligue->ligue_nom."</td>";
              echo "<td>".$ligue->ligue_sport."</td>";
              echo "<td>".$ligue->ligue_tel."</td>";
              echo '<td><div class="switch"><label><input class="ligueVerr"  id="ligue_'.$ligue->ligue_num.' " type="checkbox" '.($ligue->ligue_verr === null ? '': 'checked').'> <span class="lever"></span></label></div></td>';
              echo "</tr>";
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="card blue lighten-3 col s4">
      <div class="row">
        <div class="card-content">
          <h4 class="card-title">Salles</h4>
          <table class="striped centered">
            <thead>
            <tr>
              <th>nom</th>
              <th>place</th>
              <th>informatisé</th>
              <th>verrouiller</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($salles as $salle) {
              echo "<tr>";
              echo "<td>".$salle->salle_nom."</td>";
              echo "<td>".$salle->salle_places."</td>";
              echo "<td>".($salle->salle_informatise == 0 ? "<i class=\"material-icons\">cancel</i>" : "<i class=\"material-icons\">check_circle</i>")."</td>";
              echo '<td><div class="switch"><label><input class="salleVerr" id="salle_'.$salle->salle_num.'" type="checkbox" '.($salle->salle_verr === null ? '': 'checked').'> <span class="lever"></span></label></div></td>';
              echo "</tr>";
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo site_url()?>/../assets/js/admin_dashboard.js"></script>
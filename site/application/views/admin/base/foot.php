<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>M.AutoInit();</script>
<script src="<?php echo site_url()?>/../assets/js/disconnect.js"></script>
</body>
</html>

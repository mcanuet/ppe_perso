<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>
<nav class="col xs-12">
    <div class="nav-wrapper blue darken-3">
        <span class="brand-logo center">Bienvenue <?php echo $this->session->user['Pseudo'] ?></span>
        <ul class="right black-text">
            <li><a href="<?php echo site_url("Admin/dashboard")?>"> <i class="material-icons left tooltipped" data-position="bottom" data-tooltip="Tableau de bord">view_list</i></a></li>
            <li><a href="<?php echo site_url("Admin/management")?>"><i class="material-icons left tooltipped" data-position="bottom" data-tooltip="Gestion des entitées">settings</i></a></li>
            <li><a href="#" id="deco"><i class="material-icons lef tooltipped" data-position="bottom" data-tooltip="Déconnexion">lock_open</i></a></li>
        </ul>
    </div>
</nav>

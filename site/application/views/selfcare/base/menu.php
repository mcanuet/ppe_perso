<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>
<nav>
    <div class="nav-wrapper amber darken-2">
        <span class="brand-logo center">Bienvenue <?php echo $this->session->user['Pseudo'] ?></span>
        <ul class="right black-text">
            <li><a href="<?php echo site_url("Selfcare/AllSalles")?>"><i class="material-icons left tooltipped" data-position="bottom" data-tooltip="liste des salles">view_list</i></a></li>
            <li><a href="<?php echo site_url("Selfcare/reservation")?>"><i class="material-icons left tooltipped" data-position="bottom" data-tooltip="Réserver une salle">add_box</i></a></li>
            <li><a href="<?php echo site_url("Selfcare/settings")?>"><i class="material-icons left tooltipped" data-position="bottom" data-tooltip="mon compte">settings</i></a></li>
            <li><a href="#" id="deco"><i class="material-icons left tooltipped" data-position="bottom" data-tooltip="Déconnexion">lock_open</i></a></li>
        </ul>
    </div>
</nav>

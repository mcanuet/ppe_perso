<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>

<div class="row">
    <div class="col s8 offset-s2">
        <div class="card amber darken-3">
            <div class="card-content">
                <span class="card-title">Reserver une salle</span>
                <div class="row">
                    <form>
                        <div class="col s6 input-field">
                            <label for="deb" class="black-text"> date de début</label>
                            <input type="date" value="<?php echo date("Y-m-d") ?>" name="deb" id="deb">
                        </div>
                        <div class="col s6 input-field">
                            <label for="fin" class="black-text"> date de fin</label>
                            <input type="date" value="<?php echo date("Y-m-d") ?>" name="fin" id="fin">
                        </div>
                        <div class="input field">
                        </div>
                        <div class="input-field col s4">
                            <select name="Hdeb" id="Hdeb">
                                <option value="08" selected>8H</option>
                                <option value="09">9H</option>
                                <option value="10">10H</option>
                                <option value="11">11H</option>
                                <option value="14">14H</option>
                                <option value="15">15H</option>
                                <option value="16">16H</option>
                            </select>
                            <label class="black-text">heure de début</label>
                        </div>
                        <div class="input-field col s4">
                            <select name="Hfin" id="Hfin">
                                <option value="09" selected>9H</option>
                                <option value="10">10H</option>
                                <option value="11">11H</option>
                                <option value="14">14H</option>
                                <option value="15">15H</option>
                                <option value="16">16H</option>
                                <option value="17">17H</option>
                            </select>
                            <label class="black-text">heure de fin</label>
                        </div>
                        <div class="col s4 input-field">
                            <select name="places" id="places"></select>
                            <label for="places" class="black-text">nombre de places</label>
                        </div>
                        <div class="col s6">
                            <div class="switch">
                                <label class="black-text">
                                    Non
                                    <input type="checkbox" id="info" name="info">
                                    <span class="lever"></span>
                                    Oui
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-action">
                    <div class="btn waves-effect amber accent-2  black-text" id="sub">Reserver</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url("/").'../assets/js/reservation.js'?>"></script>
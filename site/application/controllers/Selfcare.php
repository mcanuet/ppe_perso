<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxime
 * Date: 22/10/2018
 * Time: 11:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Selfcare extends CI_Controller {

  /**
   * permet d'accéder à la page de connexion
   */
    public function index(){
        $this->load->view('selfcare/base/head');
        $this->load->view('selfcare/connexion');
        $this->load->view('selfcare/base/foot');
    }

  /**
   * vérifie les informations de connexions
   */
    public function connexion(){
        $this->load->model('Connexion');
        $data = $this->Connexion->test($_POST['pseudo'], $_POST['pwd']);
        if ($data[0] and $data[0]->utilisateur_varr == null){
            $this->session->user = array(
                'Id' => $data[0]->utilisateur_num,
                'Pseudo' => $data[0]->utilisateur_identifiant,
                'IsAdmin' => ($data[0]->utilisateur_admin == '0') ? 0 : 1,
                'IsResponssable' => ($data[0]->utilisateur_responssable == '0') ? 0 : 1,
                'LigueId' => $data[0]->ligue_num
                );
            redirect(site_url('selfcare/AllSalles'));
        }
        else{
            redirect(site_url('/'));
        }
    }

  /**
   * permet d'accerder au tableau de bord
   */
    public function AllSalles(){
        if (!isset($this->session->user)){redirect(site_url('/'));}
        $this->load->model('Getter');
        $data['salles'] = $this->Getter->AllSalles();
        $data['reservations'] = $this->Getter->AllReservationOneUser($this->session->user['LigueId']);
        $this->load->view('selfcare/base/head');
        $this->load->view('selfcare/base/menu');
        $this->load->view('selfcare/listing', $data);
        $this->load->view('selfcare/base/foot');
    }

  /**
   * permet d'acceder au formulaire de reservation
   */
    public function reservation() {
        if (!isset($this->session->user)){redirect(site_url('/'));}
        $this->load->view('selfcare/base/head');
        $this->load->view('selfcare/base/menu');
        $this->load->view('selfcare/reservation');
        $this->load->view('selfcare/base/foot');
    }

    public function settings() {
        if (!isset($this->session->user)){redirect(site_url('/'));}
        $this->load->view('selfcare/base/head');
        $this->load->view('selfcare/base/menu');
        $this->load->view('selfcare/settings');
        $this->load->view('selfcare/base/foot');
    }
}

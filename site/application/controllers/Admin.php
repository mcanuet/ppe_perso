<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxime
 * Date: 27/10/2018
 * Time: 14:09
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Admin
 */
class Admin extends CI_Controller{

  /**
   * index si rien n'est spécifier dans l'uri cette méthode est exécuté
   */
  public function index(){
    $this->load->view('admin/base/head');
    $this->load->view('admin/connexion');
    $this->load->view('admin/base/foot');
  }

  /**
   * methode appelé lors de la connexion d'un utilisateur
   */
  public function connexion(){
    $this->load->model('Connexion');
    $data = $this->Connexion->test($_POST['pseudo'], $_POST['pwd']);
    if ($data[0] && $data[0]->utilisateur_admin === "1"){
      $this->session->user = array(
        'Id' => $data[0]->utilisateur_num,
        'Pseudo' => $data[0]->utilisateur_identifiant,
        'IsAdmin' => $data[0]->utilisateur_admin
      );
      redirect(site_url('admin/dashboard'));
    }
    else{
      redirect(site_url('/admin'));
    }
  }

  /*
   * permet d'accéder au tableau de bord utilisateur
   */
  public function dashboard() {
    if (!isset($this->session->user) && !isset($this->session->user['IsAdmin'])){redirect(site_url('/admin'));}
    $this->load->model('Getter');
    $data['ligues']=$this->Getter->AllLigues();
    $data['salles']=$this->Getter->AllSalles();
    $data['users']=$this->Getter->AllUsers();
    $this->load->view('admin/base/head');
    $this->load->view('admin/base/menu');
    $this->load->view('admin/dashboard', $data);
    $this->load->view('admin/base/foot');
  }

  public function management(){
      $this->load->view('admin/base/head');
      $this->load->view('admin/base/menu');
      $this->load->view('admin/management');
      $this->load->view('admin/base/foot');
  }
}
<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/11/2018
 * Time: 14:06
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller pour les requettes ajax
 * Class Ajax
 */
class Ajax extends CI_Controller
{

  /**
   * destruction de la session d'un utilisateur
   */
    public function disconnect() {
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        echo session_destroy();
    }

  /**
   * permet de verrouiller un utilisateur, une salle ou une ligue
   * @return int retourne le code html de la requête
   */
    public function setVerr()
    {
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        if (!isset($_POST['qui']) and (!isset($_POST['userId']) or !isset($_POST['ligueId']) or !isset($_POST['salleId']))){return http_response_code(400);}
        $this->load->model("Setter");
        switch ($_POST['qui']){
            default:
                return http_response_code(400);
                break;
            case 'user':
                if ($_POST['value'] == 1){
                    $this->Setter->verrUser($_POST['userId']);
                    return http_response_code(204);

                } else {
                    $this->Setter->deverrUser($_POST['userId']);
                    return http_response_code(204);
                }

            case 'ligue':
                if ($_POST['value'] == 1){
                    $this->Setter->verrLigue($_POST['ligueId']);
                } else {
                    $this->Setter->deverrLigue($_POST['ligueId']);
                }
                return http_response_code(204);

            case 'salle':
                if ($_POST['value'] == 1){
                    $this->Setter->verrSalle($_POST['salleId']);
                } else {
                    $this->Setter->deverrSalle($_POST['salleId']);
                }
                return http_response_code(204);
        }
    }

  /**
   * methode permettant de donner ou retirer les droits administrateurs d'un utilisateur
   * @return int code http de la requette
   */
    public function setAdmin(){
	header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        if (!isset($_POST['userId']) and !isset($_POST['value'])){return http_response_code(400);}
        $this->load->model("Setter");
        ($_POST['value'] == 1)? $this->Setter->setAdmin($_POST['userId']) : $this->Setter->removeAdmin($_POST['userId']);
        return http_response_code(200);
    }

  /**
   * methode permettant de donner ou retirer les droits de responssable d'un utilisateur
   * @return int code http de la requête
   */
    public function setResp(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        if (!isset($_POST['userId']) and !isset($_POST['value'])){return http_response_code(400);}
        $this->load->model("Setter");
        ($_POST['value'] == 1)? $this->Setter->setResp($_POST['userId']) : $this->Setter->removeResp($_POST['userId']);
        return http_response_code(200);
    }

  /**
   * methode perméttant de reserver une salle
   * @return int code http de la requête
   */
    public function makeRes(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsResponssable'] != '1' or !isset($this->session->user['LigueId'])){return http_response_code(401);}
        if (!isset($_POST['deb']) and !isset($_POST['fin']) and !isset($_POST['Hdeb']) and !isset($_POST['Hfin']) and !isset($_POST['info'])){return http_response_code(400);}
        $this->load->model("Inserter");
        $debut = "'".$_POST['deb']." ".$_POST['Hdeb'].":00:00'";
        $fin = "'".$_POST['fin']." ".$_POST['Hfin'].":00:00'";
        $res = $this->Inserter->makeReservation($_POST['info'], $debut, $fin, $_POST['places']);
        if ($res === true) echo json_encode(array("status"=>$res));
        if ($res === false) return http_response_code("500");
    }

  /**
   * méthode affichant la liste des ligues
   * @return int code http de la requête
   */
    public function getLigues(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        $this->load->model('Getter');
        echo json_encode($this->Getter->AllLigues());
    }

  /**
   * méthode affichant la liste des utilisateurs
   * @return int code http de la requête
   */
    public function getUsers(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        $this->load->model('Getter');
        echo json_encode($this->Getter->AllUsers());
    }

  /**
   * méthode affichant la liste des salles
   * @return int code http de la requête
   */
    public function getSalles(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if (!isset($this->session->user)){return http_response_code(401);}
        $this->load->model('Getter');
        echo json_encode($this->Getter->AllSalles());
    }

  /**
   * méthode affichant une salle
   * @return int code http de la requête
   */
    public function getOneSalle(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if (!isset($this->session->user)){return http_response_code(401);}
        $this->load->model('Getter');
        echo json_encode($this->Getter->OneSalle($_GET['id']));
    }

  /**
   * méthode affichant une ligue
   * @return int code http de la requête
   */
  public function getOneLigue(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
	if (!isset($this->session->user)){return http_response_code(401);}
    	$this->load->model('Getter');
    	echo json_encode($this->Getter->OneLigue($_GET['id']));
  }

  /**
   * méthode affichant un utilisateur
   * @return int code http de la requête
   */
  public function getOneUser(){
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
    if (!isset($this->session->user)){return http_response_code(401);}
    $this->load->model('Getter');
    echo json_encode($this->Getter->OneUser($_GET['id']));
  }

  /**
   * méthode pérmettant de créer un utilisateur ou une ligue ou une salle
   * @return int code http de la requête
   */
    public function createEntity()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        if (!isset($_POST['what'])){return http_response_code(400);}
        $this->load->model("Inserter");
        switch ($_POST['what']){
            default:
                return http_response_code(400);
                break;
            case 'user':
                if (isset($_POST['user_name']) and isset($_POST['user_mdp']) and isset($_POST['user_ligue']) and isset($_POST['user_responssable']) and isset($_POST['user_admin'])){
                    if ($_POST['user_ligue'] == 'null'){$_POST['user_ligue'] = null;}
                    $this->Inserter->createUser($_POST['user_name'], $_POST['user_mdp'], $_POST['user_ligue'], $_POST['user_responssable'], $_POST['user_admin']);
                    return http_response_code(204);
                }
                return http_response_code(400);

            case 'ligue':
                if (isset($_POST['ligue_name']) and isset($_POST['ligue_sport']) and isset($_POST['ligue_tel'])){
                    $this->Inserter->createLigue($_POST['ligue_name'] , $_POST['ligue_sport'], $_POST['ligue_tel']);
                    return http_response_code(204);
                }
                return http_response_code(400);

            case 'salle':
                if (isset($_POST['salle_name']) and isset($_POST['salle_num']) and isset($_POST['salle_info'])){
                    $this->Inserter->createSalle($_POST['salle_name'] , $_POST['salle_num'], $_POST['salle_info']);
                    return http_response_code(204);
                }
                return http_response_code(400);
        }
    }

  /**
   * méthode pérmettant de mettre à jour un utilisateur ou une ligue ou une salle
   * @return int code http de la requête
   */
    public function updateEntity() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
        if ($this->session->user['IsAdmin'] != '1'){return http_response_code(401);}
        if (!isset($_POST['what'])){return http_response_code(400);}
        $this->load->model("Setter");

        switch ($_POST['what']) {

            case 'user':
                if (isset($_POST['user_num']) && isset($_POST['user_name']) && isset($_POST['user_mdp']) && isset($_POST['user_admin']) && isset($_POST['user_responssable']) && isset($_POST['user_ligue'])) {
                    $this->Setter->updateUser($_POST['user_num'], $_POST['user_name'], $_POST['user_mdp'], $_POST['user_admin'], $_POST['user_responssable'], $_POST['user_ligue']);
                    return http_response_code(201);
                }
                return http_response_code(400);

            case 'salle':
                var_dump($_POST);
                if (isset($_POST['salle_num']) && isset($_POST['salle_name']) && isset($_POST['salle_place']) && isset($_POST['salle_info'])) {
                    $this->Setter->updateSalle($_POST['salle_num'], $_POST['salle_name'], $_POST['salle_place'], $_POST['salle_info']);
                    return http_response_code(201);
                }
                return http_response_code(400);

            case 'ligue':
                if (isset($_POST['ligue_num']) && isset($_POST['ligue_name']) && isset($_POST['ligue_sport']) && isset($_POST['ligue_tel'])) {
                    $_POST['ligue_name'] = str_replace('%20', ' ', $_POST['ligue_name']);
                    var_dump($_POST);
                    $this->Setter->updateLigue($_POST['ligue_num'], $_POST['ligue_name'], $_POST['ligue_sport'], $_POST['ligue_tel']);
                    return http_response_code(201);
                }
                return http_response_code(400);

            case 'reservation':
                if (isset($_POST['id']) && isset($_POST['debut']) && isset($_POST['fin']) && isset($_POST['ligue']) && isset($_POST['salle'])) {
                    $this->Setter->updateReservation($_POST['id'], $_POST['debut'], $_POST['fin'], $_POST['ligue'], $_POST['salle']);
                    return http_response_code(201);
                }
                return http_response_code(400);

            default:
                return http_response_code(400);
        }
    }

  /**
   * méthode affichant la liste des reservations
   * @return int code http de la requête
   */
    public function getReservation(){
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
      $this->load->model('Getter');
      if (!$this->session->user['IsAdmin']) {
        $res = $this->Getter->AllReservationOneUser($this->session->user['LigueId']);
      } else {
        $res = $this->Getter->AllReservation();
      }
      echo json_encode($res);
    }
}

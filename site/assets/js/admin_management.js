function refreshLigueListe() {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getLigues",
        method: "GET",
    })
    .done(function (msg) {
        msg = JSON.parse(msg);
        for (let i = 0; i < msg.length; i++){
            $('.user_ligue').append("<option value='"+msg[i]['ligue_num']+"'>"+msg[i]['ligue_nom']+"</option>")
        }
        $('select').formSelect();
    });
}

function refreshSalleListe() {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getSalles",
        method: "GET",
    })
        .done(function (msg) {
            msg = JSON.parse(msg);
            for (let i = 0; i < msg.length; i++){
                $('#salle_num').append("<option value='"+msg[i]['salle_num']+"'>"+msg[i]['salle_nom']+"</option>")
            }
            $('select').formSelect();
        });
}

function refreshUserListe() {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getUsers",
        method: "GET",
    })
        .done(function (msg) {
            msg = JSON.parse(msg);
            for (let i = 0; i < msg.length; i++){
                $('#user_num').append("<option value='"+msg[i]['utilisateur_num']+"'>"+msg[i]['utilisateur_identifiant']+"</option>")
            }
            $('select').formSelect();
        });
}
// ***************************
//  initialisation des champs
// ***************************
refreshLigueListe();
refreshSalleListe();
refreshUserListe();

$("#salle_num").change(function(e) {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getOneSalle?id="+e.target.value,
        method: "GET",
        statusCode: {
            200: function (e) {
                const data = JSON.parse(e);
                $("#salle_name")[0].value = data[0].salle_nom;
                $("#salle_place")[0].value = data[0].salle_places;
                $("#salle_info")[0].checked = data[0].salle_informatise == '1';
                M.updateTextFields();
            }
        }
    });
});

$("#ligue_num").change(function(e) {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getOneLigue?id="+e.target.value,
        method: "GET",
        statusCode: {
            200: function (e) {
                const data = JSON.parse(e);
                $("#ligue_name")[0].value = data[0].ligue_nom;
                $("#ligue_sport")[0].value = data[0].ligue_sport;
                $("#ligue_tel")[0].value = data[0].ligue_tel;
                M.updateTextFields();
            }
        }
    });
});

$("#user_num").change(function(e) {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getOneUser?id="+e.target.value,
        method: "GET",
        statusCode: {
            200: function (e) {
                const data = JSON.parse(e);
                $("#user_name")[0].value = data[0].utilisateur_identifiant;
                $("#user_admin")[0].checked = data[0].utilisateur_admin == "1";
                $("#user_responssable")[0].checked = data[0].utilisateur_responssable == "1";
                $("#user_ligue_upd")[0].value = data[0].ligue_num;

                M.updateTextFields();
                $('select').formSelect();
            }
        }
    });
});
// ***************************
//  Modification d'entité
// ***************************

$(".update").submit(function (e) {
    e.preventDefault();
    const res = $("#"+this.id).serialize().split("&");
    let val = {};
    for (let i in res){
        let a = res[i].split("=");
        val[a[0]] = a[1];
    }
    val['what'] = this.id.replace('_update','');
    (val["salle_info"] === "on")? val["salle_info"] = 1 : val["salle_info"] = 0;
    (val["user_admin"] === "on")? val["user_admin"] = 1 : val["user_admin"] = 0;
    (val["user_responssable"] === "on" && val["user_ligue"] !== 'null')? val["user_responssable"] = 1 : val["user_responssable"] = 0;
    if (val["user_mdp"]) val["user_mdp"] = Crypto.SHA1(val["user_mdp"]);
    console.log(val);
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/updateEntity",
        method: "POST",
        data: val,
        statusCode: {
            201: function () {
                M.toast({html: "Entité modifié", class: 'rounded'});
            },
            500: function () {
                M.toast({html: "L'entité n'à pas pue être modifiée pour ces critères", class: 'rounded'});
            },
            401: function () {
                M.toast({html: "Vous n'êtes pas authorisé à modifier d'entité", class: 'rounded'})
            },
            400: function () {
                M.toast({html: "Un ou plusieurs champs contients de mauvaises informations", class: 'rounded'})
            }
        }
    })
});

// ***************************
//  Suppression d'entité
// ***************************

// ***************************
//  Création d'entité
// ***************************

$(".create").submit(function (e) {
    e.preventDefault();
    const res = $("#"+this.id).serialize().split("&");
    let val = {};
    for (let i in res){
        let a = res[i].split("=");
        val[a[0]] = a[1];
    }
    val['what'] = this.id;
    (val["salle_info"] === "on")? val["salle_info"] = 1 : val["salle_info"] = 0;
    (val["user_admin"] === "on")? val["user_admin"] = 1 : val["user_admin"] = 0;
    (val["user_responssable"] === "on" && val["user_ligue"] !== 'null')? val["user_responssable"] = 1 : val["user_responssable"] = 0;
    if (val["user_mdp"]) val["user_mdp"] = Crypto.SHA1(val["user_mdp"]);
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/createEntity",
        method: "POST",
        data: val,
        statusCode: {
            204: function () {
                M.toast({html: "Entité créée", class: 'rounded'});
            },
            500: function () {
                M.toast({html: "L'entité n'à pas pue être créée pour ces critères", class: 'rounded'});
            },
            401: function () {
                M.toast({html: "Vous n'êtes pas authorisé à créé d'entité", class: 'rounded'})
            },
            400: function () {
                M.toast({html: "Un ou plusieurs champs contien de mauvaises informations", class: 'rounded'})
            }
        }
    });
});


/*
 * Crypto-JS v2.5.3
 * http://code.google.com/p/crypto-js/
 * (c) 2009-2012 by Jeff Mott. All rights reserved.
 * http://code.google.com/p/crypto-js/wiki/License
 */
(typeof Crypto=="undefined"||!Crypto.util)&&function(){var d=window.Crypto={},m=d.util={rotl:function(a,c){return a<<c|a>>>32-c},rotr:function(a,c){return a<<32-c|a>>>c},endian:function(a){if(a.constructor==Number)return m.rotl(a,8)&16711935|m.rotl(a,24)&4278255360;for(var c=0;c<a.length;c++)a[c]=m.endian(a[c]);return a},randomBytes:function(a){for(var c=[];a>0;a--)c.push(Math.floor(Math.random()*256));return c},bytesToWords:function(a){for(var c=[],b=0,i=0;b<a.length;b++,i+=8)c[i>>>5]|=(a[b]&255)<<
        24-i%32;return c},wordsToBytes:function(a){for(var c=[],b=0;b<a.length*32;b+=8)c.push(a[b>>>5]>>>24-b%32&255);return c},bytesToHex:function(a){for(var c=[],b=0;b<a.length;b++)c.push((a[b]>>>4).toString(16)),c.push((a[b]&15).toString(16));return c.join("")},hexToBytes:function(a){for(var c=[],b=0;b<a.length;b+=2)c.push(parseInt(a.substr(b,2),16));return c},bytesToBase64:function(a){if(typeof btoa=="function")return btoa(f.bytesToString(a));for(var c=[],b=0;b<a.length;b+=3)for(var i=a[b]<<16|a[b+1]<<
        8|a[b+2],l=0;l<4;l++)b*8+l*6<=a.length*8?c.push("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(i>>>6*(3-l)&63)):c.push("=");return c.join("")},base64ToBytes:function(a){if(typeof atob=="function")return f.stringToBytes(atob(a));for(var a=a.replace(/[^A-Z0-9+\/]/ig,""),c=[],b=0,i=0;b<a.length;i=++b%4)i!=0&&c.push(("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".indexOf(a.charAt(b-1))&Math.pow(2,-2*i+8)-1)<<i*2|"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".indexOf(a.charAt(b))>>>
        6-i*2);return c}},d=d.charenc={};d.UTF8={stringToBytes:function(a){return f.stringToBytes(unescape(encodeURIComponent(a)))},bytesToString:function(a){return decodeURIComponent(escape(f.bytesToString(a)))}};var f=d.Binary={stringToBytes:function(a){for(var c=[],b=0;b<a.length;b++)c.push(a.charCodeAt(b)&255);return c},bytesToString:function(a){for(var c=[],b=0;b<a.length;b++)c.push(String.fromCharCode(a[b]));return c.join("")}}}();
(function(){var d=Crypto,m=d.util,f=d.charenc,a=f.UTF8,c=f.Binary,b=d.SHA1=function(a,l){var g=m.wordsToBytes(b._sha1(a));return l&&l.asBytes?g:l&&l.asString?c.bytesToString(g):m.bytesToHex(g)};b._sha1=function(b){b.constructor==String&&(b=a.stringToBytes(b));var c=m.bytesToWords(b),g=b.length*8,b=[],d=1732584193,h=-271733879,j=-1732584194,k=271733878,f=-1009589776;c[g>>5]|=128<<24-g%32;c[(g+64>>>9<<4)+15]=g;for(g=0;g<c.length;g+=16){for(var o=d,p=h,q=j,r=k,s=f,e=0;e<80;e++){if(e<16)b[e]=c[g+e];else{var n=
    b[e-3]^b[e-8]^b[e-14]^b[e-16];b[e]=n<<1|n>>>31}n=(d<<5|d>>>27)+f+(b[e]>>>0)+(e<20?(h&j|~h&k)+1518500249:e<40?(h^j^k)+1859775393:e<60?(h&j|h&k|j&k)-1894007588:(h^j^k)-899497514);f=k;k=j;j=h<<30|h>>>2;h=d;d=n}d+=o;h+=p;j+=q;k+=r;f+=s}return[d,h,j,k,f]};b._blocksize=16;b._digestsize=20})();

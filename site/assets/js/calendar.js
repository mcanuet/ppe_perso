$("document").ready(function () {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getReservation",
        method: "GET",
        statusCode: {
            200: function (e) {
               makeCalendar(e)
            }
        }
    })
});

/**
 * création du calendrier avec les réservations
 * @param data json contenant les réservations de l'utilisateur
 */
function makeCalendar(data) {
    let goodData = [];
    for (i of JSON.parse(data)) {
        let temp = {
            start: i.reservation_debut,
            end: i.reservation_fin,
            title: i.salle_nom,
            allDay: false
        };
        goodData.push(temp);
    }
    $('#calendar').fullCalendar({
        editable: false,
        events: goodData,
        header: {
            right: 'month, basicWeek, agenda, basicDay',
            left: 'prev, next, today',
            center: 'title'
        }
    });
}

$("#sub").click(function () {
    const res = $("form").serialize().split("&");
    let val = {};
    for (let i in res){
        let a = res[i].split("=");
        val[a[0]] = a[1];
    }
    (val["info"] === "on")? val["info"] = 1 : val["info"] = 0;
    if (val["deb"] <= val["fin"] && val["Hdeb"] < val["fin"]) {
        resSalle(val);
    } else {
        M.toast({html: "Aucune salle n'est disponible pour les critères sélectionées", class: 'rounded'});
    }
});

function resSalle(data) {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/makeRes",
        method: "POST",
        data: data,
        statusCode: {
            200: function () {
                M.toast({html: "Votre réservation est effectué", class: 'rounded'});
            },
            500: function () {
                M.toast({html: "Aucune salle n'est disponible pour les critères sélectionées", class: 'rounded'});
            },
            401: function () {
                M.toast({html: "Vous n'êtes pas authorisé à fair de réservation", class: 'rounded'})
            }
        }
    });
}

function makeRoomSizeListe() {
    $.ajax({
        url: "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/getSalles",
        method: "GET",
        statusCode: {
            200: function (e) {
                const resp = JSON.parse(e);
                let cpt = [resp[0].salle_places];
                for (let i = 1; i < resp.length; i++) {
                    if (cpt.indexOf(resp[i].salle_places) === -1){
                        cpt.push(resp[i].salle_places);
                    }
                }
                for (let i = 0; i < cpt.length; i++) {
                    $("#places").append("<option value='"+parseInt(cpt[i])+"'>"+cpt[i]+"</option>")
                }
                $('select').formSelect();
            }
        }
    });
}

makeRoomSizeListe();

var serv = "http://151.80.190.149/mcanuet/ppe_perso/site/index.php/Ajax/";

$(".userAdmin").click(function () {
    $.ajax({
        url: serv+"setAdmin",
        method: "POST",
        data: {
            'value': (this.checked === true)? 1 : 0,
            'userId': this.id.substr(5)
        }
    })
    .done(function () {
        M.toast({html: "droit administrateur modifié", class: 'rounded'});
    });
});

$(".userResp").click(function () {
    $.ajax({
        url: serv+"setResp",
        method: "POST",
        data: {
            'value': (this.checked === true)? 1 : 0,
            'userId': this.id.substr(5)
        }
    })
    .done(function () {
        M.toast({html: "droit modifé", class: 'rounded'});
    });
});

$(".userVerr").click(function () {
    $.ajax({
        url: serv+"setVerr",
        method: "POST",
        data: {
            'value': (this.checked === true)? 1 : 0,
            'qui': 'user',
            'userId': this.id.substr(5)
        }
    })
    .done(function () {
        M.toast({html: "verrouillage de l'utilisateur modifié", class: 'rounded'});
    });
});


$(".ligueVerr").click(function () {
    $.ajax({
        url: serv+"setVerr",
        method: "POST",
        data: {
            'value': (this.checked === true)? 1 : 0,
            'qui': 'ligue',
            'ligueId': this.id.substr(6)
        }
    })
    .done(function () {
        M.toast({html: "verrouillage de la ligue modifié", class: 'rounded'});
    });
});


$(".salleVerr").click(function () {
    $.ajax({
        url: serv+"setVerr",
        method: "POST",
        data: {
            'value': (this.checked === true)? 1 : 0,
            'qui': 'salle',
            'salleId': this.id.substr(6)
        }
    })
    .done(function () {
        M.toast({html: "verrouillage de la salle modifié", class: 'rounded'});
    });
});

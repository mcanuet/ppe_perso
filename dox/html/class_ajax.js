var class_ajax =
[
    [ "createEntity", "class_ajax.html#a31265ca609e83367571773a2e4db3035", null ],
    [ "disconnect", "class_ajax.html#abe175fcf658475bc56e9d6fa02bc88ec", null ],
    [ "getLigues", "class_ajax.html#ac2778a5e1033f18e0bee9061b23cca05", null ],
    [ "getOneLigue", "class_ajax.html#a151e076a051710dc48816f96c1b56b76", null ],
    [ "getOneSalle", "class_ajax.html#a8c983379b103b3b5923c93a7b097d6ed", null ],
    [ "getOneUser", "class_ajax.html#ae8bbe90560daa432c38cacc947e6a5e4", null ],
    [ "getReservation", "class_ajax.html#a4153dc5042690f7a826acf0e79278b0e", null ],
    [ "getSalles", "class_ajax.html#a70d861213f3803f3fa4df14cfb21071b", null ],
    [ "getUsers", "class_ajax.html#a0fc10b64683021b70c7eb95fb514c119", null ],
    [ "makeRes", "class_ajax.html#ab8beed1020dd876e7e687bca8fd16161", null ],
    [ "setAdmin", "class_ajax.html#aee7ff1ff6e81902ff700696c7c2b8035", null ],
    [ "setResp", "class_ajax.html#a860ddb0d7d58250ef974499b587ec83b", null ],
    [ "setVerr", "class_ajax.html#a4dcec987d789a09408e496345be7c657", null ],
    [ "updateEntity", "class_ajax.html#abe5ac96a7b95dd2f9f579d7477de3119", null ]
];
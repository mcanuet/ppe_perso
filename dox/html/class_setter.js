var class_setter =
[
    [ "deverrLigue", "class_setter.html#aadbd786877402b6d5bfb3e28f535b27c", null ],
    [ "deverrSalle", "class_setter.html#a29725a204a005f7be3095467e3be37a1", null ],
    [ "deverrUser", "class_setter.html#ace592ba9edc536dfc608b8cbf3450d40", null ],
    [ "removeAdmin", "class_setter.html#abf27a441809202bd1dea5627acb1d2e7", null ],
    [ "removeResp", "class_setter.html#a50ceca907026a9ba8475b6faed852b4b", null ],
    [ "setAdmin", "class_setter.html#a030481fb41b921f0f3d5a68815a4e9c5", null ],
    [ "setResp", "class_setter.html#a36c5e04e981323f0a30a0363f95ac939", null ],
    [ "updateLigue", "class_setter.html#a04a1e0e221244ee1aa25ba371783cfc9", null ],
    [ "updateReservation", "class_setter.html#a2fb1854df51f1b38cdf54061acdcc6e2", null ],
    [ "updateSalle", "class_setter.html#a1bc9761e48590d74ab1a4d53bfd3122f", null ],
    [ "updateUser", "class_setter.html#a5baede1d4fd61eaf2c8d85640a1c14c6", null ],
    [ "verrLigue", "class_setter.html#a02c1299adf5e355133f34dbbd7a693ae", null ],
    [ "verrSalle", "class_setter.html#ac2ea20ca9d02f34d397c794db7a5551a", null ],
    [ "verrUser", "class_setter.html#af51086eda6db7e7536fbb64ed01a5d8d", null ]
];
﻿CREATE TABLE `ppe_1_utilisateur` (
  `utilisateur_num` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_identifiant` varchar(45) NOT NULL,
  `utilisateur_admin` tinyint(4) NOT NULL,
  `utilisateur_responssable` tinyint(4) NOT NULL DEFAULT '0',
  `utilisateur_mdp` varchar(45) NOT NULL,
  `ligue_num` int(11) DEFAULT NULL,
  `utilisateur_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`utilisateur_num`),
  UNIQUE KEY `utilisateur_num_UNIQUE` (`utilisateur_num`),
  UNIQUE KEY `utilisateur_identifiant_UNIQUE` (`utilisateur_identifiant`),
  KEY `fk_ppe_1_utilisateur_ppe_1_ligue1_idx` (`ligue_num`),
  CONSTRAINT `fk_ppe_1_utilisateur_ppe_1_ligue1` FOREIGN KEY (`ligue_num`) REFERENCES `ppe_1_ligue` (`ligue_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `ppe_1_salle` (
  `salle_num` int(11) NOT NULL AUTO_INCREMENT,
  `salle_nom` varchar(45) NOT NULL,
  `salle_places` int(11) NOT NULL DEFAULT '15',
  `salle_informatise` tinyint(4) NOT NULL DEFAULT '0',
  `salle_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`salle_num`),
  UNIQUE KEY `salle_num_UNIQUE` (`salle_num`),
  UNIQUE KEY `salle_nom_UNIQUE` (`salle_nom`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

CREATE TABLE `ppe_1_reservation` (
  `reservation_num` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_demande` datetime NOT NULL,
  `reservation_debut` datetime NOT NULL,
  `reservation_fin` datetime NOT NULL,
  `salle_num` int(11) NOT NULL,
  `ligue_num` int(11) NOT NULL,
  PRIMARY KEY (`reservation_num`,`salle_num`,`ligue_num`),
  UNIQUE KEY `reservation_num_UNIQUE` (`reservation_num`),
  KEY `fk_ppe_1_reservation_ppe_1_salle1_idx` (`salle_num`),
  KEY `fk_ppe_1_reservation_ppe_1_ligue1_idx` (`ligue_num`),
  CONSTRAINT `fk_ppe_1_reservation_ppe_1_ligue1` FOREIGN KEY (`ligue_num`) REFERENCES `ppe_1_ligue` (`ligue_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ppe_1_reservation_ppe_1_salle1` FOREIGN KEY (`salle_num`) REFERENCES `ppe_1_salle` (`salle_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `ppe_1_ligue` (
  `ligue_num` int(11) NOT NULL AUTO_INCREMENT,
  `ligue_nom` varchar(45) NOT NULL,
  `ligue_sport` varchar(45) NOT NULL,
  `ligue_tel` varchar(45) NOT NULL,
  `ligue_verr` datetime DEFAULT NULL,
  PRIMARY KEY (`ligue_num`),
  UNIQUE KEY `ligue_num_UNIQUE` (`ligue_num`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
